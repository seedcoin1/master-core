﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RocketCore
{
    [Serializable] //TODO check absence
    class Account
    {
        internal decimal AvailableFunds1 { get; set; } //свободные средства currency1
        internal decimal BlockedFunds1 { get; set; } //заблокированные в заявках средства currency1
        internal decimal AvailableFunds2 { get; set; } //свободные средства currency2
        internal decimal BlockedFunds2 { get; set; } //заблокированные в заявках средства currency2        
        internal decimal Fee { get; set; } //индивидуальная комиссия
        internal decimal MarginLevel { get; set; } //уровень маржи
        internal bool MarginCall { get; set; } //флаг Margin Call
        internal bool Suspended { get; set; } //флаг заморозки счёта

        internal Account()
        {
            AvailableFunds1 = 0;
            BlockedFunds1 = 0;
            AvailableFunds2 = 0;
            BlockedFunds2 = 0;            
            Fee = 0.002m;
            MarginLevel = 1m;
            MarginCall = false;
            Suspended = false;
        }

        internal Account(decimal af1, decimal bf1, decimal af2, decimal bf2, decimal fee)
        {
            AvailableFunds1 = af1;
            BlockedFunds1 = bf1;
            AvailableFunds2 = af2;
            BlockedFunds2 = bf2;
            Fee = fee;
            MarginLevel = 1m;
            MarginCall = false;
            Suspended = false;
        }
    }
}
